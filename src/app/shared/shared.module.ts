import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { ToTimePipe } from './pipes/to-time.pipe';



@NgModule({
  declarations: [ HeaderComponent, ToTimePipe ],
  imports: [
    CommonModule
  ], exports: [ HeaderComponent, ToTimePipe ]
})
export class SharedModule { }
