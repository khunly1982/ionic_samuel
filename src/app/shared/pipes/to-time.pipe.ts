import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'toTime'
})
export class ToTimePipe implements PipeTransform {

  transform(value: number, ...args: unknown[]): string {

    let hours = Math.floor(value / 3600);
    let minutes = Math.floor((value % 3600) / 60);
    let sec = Math.floor(value % 60);
    return `${(hours >= 10) ? hours : '0' + hours }:${(minutes >= 10) ? minutes : '0' + minutes }:${(sec >= 10) ? sec : '0' + sec }`;
  }

}
