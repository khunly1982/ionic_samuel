import { Component, OnInit } from '@angular/core';
import { ActionSheetController, AlertController } from '@ionic/angular';
import { Article } from '../models/article';

@Component({
  selector: 'app-ex3',
  templateUrl: './ex3.page.html',
  styleUrls: ['./ex3.page.scss'],
})
export class Ex3Page implements OnInit {

  newArticle: string;

  articles: Article[];

  constructor(
    private actionSheetCtrl: ActionSheetController,
    private alertController: AlertController
  ) { }

  ngOnInit() {
    this.articles = [];
  }

  add() {
    this.articles.push({ name: this.newArticle, isChecked: false });
    this.newArticle = null;
  }

  async openActions(item: Article) {
    let a = await this.actionSheetCtrl.create({
      header: 'Actions sur ' + item.name,
      buttons: [
        { text: 'Supprimer', 
          handler: async () => {
            let alert = await this.alertController.create({ 
              header: 'Êtes vous sur ... ?',
              buttons: [
                { text: 'non' },
                { text: 'oui', handler: () => {
                  this.articles = this.articles.filter(x => x !== item); 
                } },
              ]
            });
            alert.present();
          },
          icon: 'trash'
        },
        { text: item.isChecked ? 'Uncheck' : 'Check', handler: () => { item.isChecked = !item.isChecked }, icon: 'checkmark' },
        { text: 'Cancel', icon: 'close' }
      ]
    });
    a.present();
  }

}
