import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ex2',
  templateUrl: './ex2.page.html',
  styleUrls: ['./ex2.page.scss'],
})
export class Ex2Page implements OnInit {

  timerId: any;

  compt: number;

  constructor() { }

  ngOnInit() {
    this.compt = 0;
  }

  start() {
    this.timerId = setInterval(() => {
      this.compt++;
    }, 1000);
  }

  stop() {
    clearInterval(this.timerId);
    this.timerId = null;
  }

  reset() {
    this.stop();
    this.compt = 0;
  }

}
