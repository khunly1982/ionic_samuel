import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    // { title: 'Inbox', url: '/folder/Inbox', icon: 'mail' },
    { title: 'Home', url: '/home', icon: 'home' },
    { title: 'Settings', url: '/settings', icon: 'cog' },
    { title: 'Exo 1', url: '/ex1', icon: 'star' },
    { title: 'Exo 2', url: '/ex2', icon: 'book' },
    { title: 'Exo 3', url: '/ex3', icon: 'cart' },
  ];
  constructor() {}
}
