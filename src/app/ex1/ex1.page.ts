import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ex1',
  templateUrl: './ex1.page.html',
  styleUrls: ['./ex1.page.scss'],
})
export class Ex1Page implements OnInit {

  compt: number; 

  constructor() { }

  ngOnInit() {
    this.compt = 0;
  }

  increase() {
    this.compt++;
  }

  decrease() {
    this.compt--;
  }

}
